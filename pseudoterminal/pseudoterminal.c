//Bibliotecas de entrada
#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include <string.h>
#include <time.h>

#define lista_de_comandos "commands.dat" //Comandos de entrada
#define lista_de_saida "saida.dat" //Comandos de saída
#define num_i_cmd 8 // numero de linhas da matriz
#define num_j_cmd 16 // numero de colunas da matriz 

//Função que limpa o terminal
void clearscreen ()
{
    const char*CLEAR_SCREEN_ANSI = "\e[1;1H\e[2J~$ ";
    write(STDOUT_FILENO, CLEAR_SCREEN_ANSI, 12);
}

// Função que retorna a quantidade de caracteres iguais entre duas palavras
int contador(char*input, char*cmd)
{   
    char cpinput[num_j_cmd], cpcmd[num_j_cmd];
    int i, caracter_cmd, caracter_input, cont;
    
    cont = 0;
    caracter_cmd=0;
    caracter_input=0;

    memset(cpcmd, '\0', num_j_cmd);
    memset(cpinput, '\0', num_j_cmd);

    strcpy(cpcmd, cmd);
    strcpy(cpinput, input);
    
    //Compara cada caracter do cpinput e do cpcmd
    for(i=0; i<num_j_cmd; i++)
    {   
        caracter_input = cpinput[i]; //Ler cada caracterer do comando de entrada
        caracter_cmd = cpcmd[i]; //Ler cada caracter do comando de saida
        
        if((caracter_input==caracter_cmd) && (caracter_cmd!=0))
        {
            cont += 1;
        }
    }
    return cont;
}

//Função que calcula e retorna o valor do cosseno
float calculaCosseno(char*input, char*cmd)
{
    char cpinput[num_j_cmd], cpcmd[num_j_cmd];
    int i, soma_cmd, soma_input, cont;
    float numerador, denominador1, denominador2, cosseno;
    
    numerador=0;
    denominador1=0;
    denominador2=0;

    //zerando as colunas das variáveis criadas, no instante 0
    memset(cpcmd, '\0', num_j_cmd);
    memset(cpinput, '\0', num_j_cmd);

    strcpy(cpcmd, cmd);
    strcpy(cpinput, input);
    //printf("O comando copiado eh: %s\n", cpinput);
    printf("O comando copiado da lista eh: %s\n", cpcmd);
    
    //Permuta as colunas do arquivo de entrada
    for(i=0; i<num_j_cmd; i++)
    {
        numerador += cpinput[i] * cpcmd[i];
        denominador1 += pow(cpinput[i], 2);
        denominador2 += pow(cpinput[i], 2);
    }

    cosseno = numerador / ( sqrt(denominador1) * sqrt(denominador2) );
    return cosseno;
}

int main()
{
    clock_t begin = clock();//Marca o tempo inicial 

    FILE *arquivo_in, *arquivo_out;

    char cmd[num_i_cmd][num_j_cmd], input[num_j_cmd], linha, comando[num_j_cmd];
    int code[num_i_cmd];
    char copycmd[num_j_cmd], copyinput[num_j_cmd];
    float cosseno[num_i_cmd];
    int contagem[num_i_cmd], cont_anterior[0];//Variáveis ligadas ao processo de contagem(comparação de caracteres)

    int confirma, i, j;

    arquivo_in = fopen(lista_de_comandos, "r");

    i=0;
    cont_anterior[0] = 0;
    linha = fscanf(arquivo_in, "%s %X", cmd[i], &code[i] );

    //Ler o arquivo de entrada linha por linha, até o final.
    while (linha !=EOF)
    {
        i++;
        linha = fscanf(arquivo_in, "%s %X", cmd[i], &code[i] );
    }

    clearscreen();

    //Estrutura principal de código do pseudoterminal
    while(1)
    {
        //printf("Digite o comando: ");
        scanf("%s", input);
        //printf("\n O comando digitado foi: %s \n\n", input);

        //Permuta as linhas do arquivo de entrada
        for(i=0; i<num_i_cmd; i++)
        {
            cosseno[i] = calculaCosseno(input, cmd[i]);
            printf("comando %d tem o cosseno igual: %.2f \n\n", i, cosseno[i]*100);
            contagem[i] = contador(input, cmd[i]);
            //printf("Contador igual a: %d \n\n", contagem[i]);

            //Pesquisando o comando que o usuário quer
            if(contagem[i] >= cont_anterior[0])
            {
                cont_anterior[0] = contagem[i];
                strcpy(comando, cmd[i]);//Atribui a palavra do ponteiro cmd para a variável "comando"
            }
        }

        printf("O comando mais proximo encontrado foi: %s \n\n", comando);//Informa o comando que o usuário quis digitar

        clock_t end = clock();//Marca o tempo final 

        double tempo = ((double) (end-begin)/CLOCKS_PER_SEC)/1000; //Calcula o tempo em segundos
        printf("O tempo decorrido foi de: %.3f segundos.\n\n ", tempo);   
        }     
}